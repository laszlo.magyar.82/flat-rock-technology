<?php

namespace Tests\Unit;

use App\Models\Result;
use PHPUnit\Framework\TestCase;

class ResultTest extends TestCase
{
    public function test_example()
    {
        $result = new Result();
        $result->correct_answers = 2;
        $result->questions_count = 3;
        $this->assertEquals(round(2 / 3 * 100, 2), $result->getPercent());
    }
}
