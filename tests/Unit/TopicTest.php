<?php

namespace Tests\Unit;

use App\Models\Topic;
use PHPUnit\Framework\TestCase;

class TopicTest extends TestCase
{
    public function test_session_key()
    {
        $topic = new Topic(['title'=>'Title']);
        $topic->id = 11;

        $this->assertEquals('startTopic_11_11',$topic->getSessionKey(11));
    }
}
