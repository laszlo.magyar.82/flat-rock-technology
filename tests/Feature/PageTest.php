<?php

namespace Tests\Feature;

use App\Models\Option;
use App\Models\Question;
use App\Models\Topic;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PageTest extends TestCase
{
    use RefreshDatabase;

    private User $user;
    private User $admin;

    public function urlProvider()
    {
        return [
            ['/users', 302, 200],
            ['/users/1', 302, 200],
            ['/users/1/edit', 302, 200],
            ['/options', 302, 200],
            ['/options/1', 302, 200],
            ['/options/1/edit', 302, 200],
            ['/questions', 302, 200],
            ['/questions/1', 302, 200],
            ['/questions/1/edit', 302, 200],
            ['/topics', 200, 200],
            ['/topics/create', 302, 200],
            ['/topics/1/edit', 302, 200],
            ['/results', 302, 200],
            ['/results/score', 200, 200],
        ];
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->admin = User::factory()->create();
        $this->admin->role = 'admin';
        $topic = new Topic(['title' => 'Title']);
        $topic->save();
        $question = new Question(['text' => 'Question', 'topic_id' => $topic->id]);
        $question->save();
        $option = new Option(['text' => 'Question', 'question_id' => $question->id, 'option' => 'Option']);
        $option->save();
    }

    /**
     * A basic feature test example.
     * @dataProvider urlProvider
     * @return void
     */
    public function testAdminPages(string $url, int $userStatusCode, int $adminStatusCode)
    {
        $this->actingAs($this->user);
        $response = $this->get($url);

        $response->assertStatus($userStatusCode);

        $this->actingAs($this->admin);
        $response = $this->get($url);

        $response->assertStatus($adminStatusCode);
    }
}
