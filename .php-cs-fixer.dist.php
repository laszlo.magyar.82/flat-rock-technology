<?php

$finder = (new PhpCsFixer\Finder())
  ->in(['app','resources'])
  ->exclude('storage');

return (new PhpCsFixer\Config())
  ->setRules([
               '@Symfony' => true,
             ])
  ->setFinder($finder);
