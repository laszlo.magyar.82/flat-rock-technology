<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit topic') }} : {{$topic->title}}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="container">
                    @if($topic)
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <hr>
                                    <form action="{{route('topics.update', $topic->id)}}" method="post">
                                        @csrf
                                        @method('put')
                                        <div class="form-group">
                                            <input required type="text" name="title" class="form-control"
                                                   value="{{$topic->title}}">
                                        </div>
                                        <input type="submit" name="submit" id="submit" class="btn btn-info"
                                               value="Update Topic Title">
                                    </form>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <hr>
                                    <form action="{{route('questions.store')}}" method="post">
                                        @csrf
                                        <input type="hidden" name="topic" id="inputState" value="{{$topic->id}}">
                                        <div>
                                            <h2 class="mt-2" align="center">Add new question</h2>
                                            <table class="table table-bordered " id="dynamic_field">
                                                <thead>
                                                <tr>
                                                    <th>Question</th>
                                                    <th>Answer options</th>
                                                    <th>Correct</th>
                                                    <th>Binary</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="text" name="question" placeholder="Question"
                                                               class="form-control question_list" required
                                                        />
                                                    <td>
                                                        <input type="text" name="options[]" placeholder="Answer"
                                                               class="form-control options_list question-answer"
                                                               required
                                                        />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox"
                                                               name="correct[]"
                                                               value="1"
                                                               placeholder="Is correct"
                                                               class="form-control"
                                                        />
                                                    </td>
                                                    <td>
                                                        <input type="checkbox"
                                                               name="binary[]"
                                                               value="1"
                                                               placeholder="Is binary"
                                                               class="cbx-binary form-control"
                                                        />
                                                    </td>
                                                    </td>
                                                    <td>
                                                        <button type="button" name="addAnswer" id="addAnswer"
                                                                class="btn btn-success mb-2">
                                                            Add Answer
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <input type="submit" name="addQuestion" id="addQuestion"
                                                   class="btn btn-success mb-2 mr-2"
                                                   value="Add Question"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">

                            function getTemplate(n) {
                                return '<tr id="row' + n + '" class="dynamic-added">' +
                                    '<td>' +
                                    '</td>' +
                                    '<td>' +
                                    '<input type="text" name="options[]" required placeholder="Other answer" class="form-control question_list question-answer" />' +
                                    '</td>' +
                                    '<td>' +
                                    '<input type="checkbox" name="correct[]" value="' + n + '" class="form-control question_list" />' +
                                    '</td>' +
                                    '<td>' +
                                    '<input type="checkbox" name="binary[]" value="' + n + '" class="cbx-binary form-control question_list" />' +
                                    '</td>' +
                                    '<td>' +
                                    '<button type="button" name="remove" id="' + n + '" class="btn btn-danger btn_remove">X</button>' +
                                    '</td>' +
                                    '</tr>';
                            }

                            function updateBinaryCheckBox(element) {
                                let checked = $(element).prop('checked');
                                if (checked) {
                                    let value = 'True';
                                    if ($('#dynamic_field tbody tr').length == 1) {
                                        $('#dynamic_field tbody').append(getTemplate(2));
                                    }
                                    $('#dynamic_field tbody tr').each(function (index, element) {
                                        console.log(index);
                                        if (index == 1) {
                                            value = 'False';
                                        } else {
                                            if (index > 1) {
                                                $(element).remove();

                                                return;
                                            }
                                        }

                                        $(element).find('input[type="text"].question-answer').prop('readonly', true);
                                        $(element).find('input[type="text"].question-answer').val(value);
                                        $('.cbx-binary').prop('checked', true);
                                    });

                                    $('#addAnswer').addClass('hidden');
                                } else {
                                    $('#addAnswer').removeClass('hidden');
                                    $('#dynamic_field tbody tr').each(function (index, element) {
                                        $(element).find('input[type="text"].question-answer').prop('readonly', false);
                                        $(element).find('.cbx-binary').prop('checked', false);
                                    });
                                    n = 2;
                                }
                            }

                            $(document).ready(function () {
                                var n = 1;

                                $('#addAnswer').click(function () {
                                    n++;
                                    let template = getTemplate(n);
                                    $('#dynamic_field tbody').append(template);
                                });

                                $('.cbx-binary').on('click', function (e) {
                                    updateBinaryCheckBox(this)
                                })

                                $(document).on('click', '.btn_remove', function () {
                                    var button_id = $(this).attr("id");
                                    $('#row' + button_id + '').remove();
                                });

                            });
                        </script>
                    @else
                        <h1>No Topic</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
