<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Topics') }}

            @if(Auth::user())
                @if(Auth::user()->role == 'admin')
                    <a class="text-sm text-gray-600 hover:text-gray-900" href="{{ route('topics.create') }}">
                        <x-button class="ml-4">
                            {{ __('Create new topic') }}
                        </x-button>
                    </a>
                @endif
            @endif

        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="d-flex" id="wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 mt-4 mb-4">
                                @if(count($topics))
                                    @foreach($topics as $topic)
                                        <div class="card">
                                            <div class="card-body mb-2 inline-block">
                                                <h5 class="card-title">{{$topic->title}}</h5>
                                                <a href="{{route('topics.show', $topic->id)}}"
                                                   class="inline-block btn btn-primary">Go
                                                    To Quiz</a>
                                                @if(Auth::user())
                                                    @if(Auth::user()->role == 'admin')
                                                        <a href="{{route('topics.edit', $topic->id)}}"
                                                           class="inline_block btn btn-warning">Edit</a>
                                                        <form class="inline-flex"
                                                              action="{{route('topics.destroy', $topic->id)}}"
                                                              method="post">
                                                            @csrf
                                                            @method('delete')
                                                            <button class="btn btn-xs btn-danger" type="submit">Delete
                                                            </button>
                                                        </form>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <h1>No Topics</h1>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
