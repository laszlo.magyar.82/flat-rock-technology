<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{$topic->title}}
        </h2>

    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="container">
                    @if($topic)
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12">
                                <div id="clockdiv"> Remaining time</div>
                                <form action="{{route('results.store')}}" method="post" id="question">
                                    @csrf
                                    <div>
                                        <input type="hidden" name="topic_id" value="{{$topic->id}}">
                                        @foreach($topic->questions as $question )
                                            <div>
                                                {{$question->text}}
                                                <input type="hidden" name="question_id[]" value="{{$question->id}}">
                                                @foreach($question->options as $option)
                                                    <div>
                                                        @if ($option->binary)
                                                            <input type="radio"
                                                                   name="option[{{$question->id}}][]"
                                                                   value="{{$option->id}}">
                                                            {{$option->option}}
                                                        @else
                                                            <input type="checkbox"
                                                                   name="option[{{$question->id}}][]"
                                                                   value="{{$option->id}}">
                                                            {{$option->option}}
                                                        @endif
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endforeach
                                    </div>
                                    <a href="{{route('results.show', $topic->id)}}"><input type="submit" value="Submit"
                                                                                           class="btn btn-success mt-3"></a>
                                </form>
                            </div>
                        </div>
                    @else
                        <h1>No Topic</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
<script>
    const deadline = '{{ $deadline }}';

    function getTimeRemaining(endtime) {
        const total = Date.parse(endtime) - Date.parse(new Date());
        const seconds = Math.floor((total / 1000) % 60);
        const minutes = Math.floor((total / 1000 / 60) % 60);
        const hours = Math.floor((total / (1000 * 60 * 60)) % 24);
        const days = Math.floor(total / (1000 * 60 * 60 * 24));

        return {total, days, hours, minutes, seconds};
    }

    function initializeClock(id, endtime) {
        const clock = document.getElementById(id);
        const timeinterval = setInterval(() => {
            const t = getTimeRemaining(endtime);
            // clock.innerHTML = 'days: ' + t.days + '<br>' +
            //     'hours: ' + t.hours + '<br>' +
            //     'minutes: ' + t.minutes + '<br>' +
            //     'seconds: ' + t.seconds;
            clock.innerHTML = ('0' + t.minutes).slice(-2) + ':' + ('0' + t.seconds).slice(-2);
            if (t.total <= 0) {
                clearInterval(timeinterval);
                clock.innerHTML = 'Running out...';
                $('form#question').submit();
            }
        }, 1000);
    }

    initializeClock('clockdiv', deadline);
</script>
<style>
    #clockdiv {
        display: inline-flex;
        float: right;
        border: 1px solid black;
        padding: 15px;
    }
</style>
