<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Option') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="d-flex" id="wrapper">
                    <div class="container">
                        @if($option)
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <hr>
                                <form action="{{route('options.update', $option->id)}}" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="table-responsive">
                                        <div class="form-group">
                                            <select name="question_id" class="form-control">
                                                @foreach($questions as $question)
                                                    <option
                                                        {{($option->question_id == $question->id) ? 'selected' : ''}} value="{{$question->id}}">{{$question->text}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" required class="form-control options_list" name="option"
                                                   id="" value="{{$option->option}}"/>
                                        </div>
                                        <div>
                                            Correct
                                            <input type="checkbox"
                                                   name="correct"
                                                   value="1"
                                                {{($option->correct == 1) ? 'checked' : ''}}
                                            />
                                        </div>
                                        <div class="mt-3">
                                            <input class="btn btn-primary" type="submit" value="Update">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        @else
                            <h1>No Question Option</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
