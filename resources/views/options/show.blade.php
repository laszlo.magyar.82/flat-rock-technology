<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Option ID: {{$option->id}}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="d-flex" id="wrapper">
                <div class="d-flex" id="wrapper">
                    <div class="container">
                        @if($option)
                            <div class="row">
                                <div class="col-md-12 mt-4">
                                    <table class="table table-bordered table-striped datatable">
                                        <thead>
                                        <tr>
                                            <th>Question</th>
                                            <th>Question option</th>
                                            <th>Correct</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>
                                                @if($option->question)
                                                    {{$option->question->text}}
                                                @endif
                                            </td>
                                            <td>{{$option->option}}</td>
                                            <td>
                                                @if($option->correct == 1)
                                                    Yes
                                                @else
                                                    No
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @else
                            <h1>No Option</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
