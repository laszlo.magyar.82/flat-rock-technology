<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('All Options') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="d-flex" id="wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 mt-4">
                                <h3 class="page-title"></h3>
                                <table class="table table-bordered table-striped datatable">
                                    <thead>
                                    <tr>
                                        <th>Question</th>
                                        <th>Question option</th>
                                        <th>Correct</th>
                                        <th>More</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($options as $option)
                                        <tr>
                                            <td>
                                                @if($option->question)
                                                    {{$option->question->text}}
                                                @endif
                                            </td>
                                            <td>{{$option->option}}</td>
                                            <td>
                                                @if($option->correct == 1)
                                                    Yes
                                                @else
                                                    No
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{route('options.show', $option->id)}}"
                                                   class="btn btn-xs btn-primary">View</a>
                                                <a href="{{route('options.edit', $option->id)}}"
                                                   class="btn btn-xs btn-warning">Edit</a>
                                                <form class="inline-flex"
                                                      action="{{route('options.destroy', $option->id)}}"
                                                      method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
