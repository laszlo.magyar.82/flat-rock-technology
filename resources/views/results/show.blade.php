<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Result') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="d-flex" id="wrapper">
                    <div class="container">
                        @if($result)
                            <div class="row">
                                <div class="col-md-12 mt-4">
                                    <table class="table table-bordered table-striped">
                                        <tr>
                                            <th>User</th>
                                            <td>{{$result->user->name}} ({{$result->user->email}})</td>
                                        </tr>
                                        <tr>
                                            <th>Date</th>
                                            <td>{{$result->created_at}}</td>
                                        </tr>
                                        <tr>
                                            <th>Score</th>
                                            <td>{{$result->getPercent()}}%</td>
                                        </tr>
                                        <tr>
                                            <th>Unanswered questions</th>
                                            <td>{{$result->unanswered_questions}}</td>
                                        </tr>
                                        <tr>
                                            <th>Time</th>
                                            <td>{{date('i:s', strtotime($result->finished_at))}}</td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered table-striped">
                                        <?php $n = 0; ?>
                                        @foreach($result->topic->questions as $question)
                                            <?php ++$n; ?>
                                            <tr class="test-option-false">
                                                <th style="width: 10%">Question #{{$n}}</th>
                                                <th>{{$question->text}}</th>
                                            </tr>
                                            <tr>
                                                <td>Options</td>
                                                <td>
                                                    <ul>
                                                        @foreach($question->options as $option)
                                                            @if($option->correct == 1)
                                                                <li style="font-weight: bold;">{{$option->option}}
                                                                    <em>(correct answer)</em>
                                                                    @foreach($result->options as $user_option)
                                                                        @if($user_option->option_id == $option->id)
                                                                            <em>(your answer)</em>
                                                                        @endif
                                                                    @endforeach
                                                                </li>
                                                            @else
                                                                <li>
                                                                    {{$option->option}}
                                                                    @foreach($result->options as $user_option)
                                                                        @if($user_option->option_id == $option->id)
                                                                            <em style="font-weight: bold;">(your
                                                                                answer)</em>
                                                                        @endif
                                                                    @endforeach
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        @else
                            <h1>No Result</h1>
                        @endif
                        <div class="row">
                            <div class="col-md-12 mt-3 mb-3">
                                <a class="text-sm text-gray-600 hover:text-gray-900 float-right"
                                   href="{{ route('topics.show',['topic'=>$result->topic_id]) }}">
                                    <x-button class="ml-4">
                                        {{ __('Restart') }}
                                    </x-button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>



