<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('All Results') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="d-flex" id="wrapper">
                    <div class="container">
                        <div class="page-container">
                            <div class="page-content-wrapper">
                                <div class="container-fluid">
                                    <div class="page-content">
                                        <div class="row">
                                            <div class="col-md-12 mt-4">
                                                <div class="panel panel-default">
                                                    <div class="panel-body">
                                                        <table class="table table-bordered table-striped datatable">
                                                            <thead>
                                                            <tr>
                                                                <th>User</th>
                                                                <th>Topic</th>
                                                                <th>Date</th>
                                                                <th>Result</th>
                                                                <th>Time</th>
                                                                <th>&nbsp;</th>
                                                            </tr>
                                                            </thead>

                                                            <tbody>
                                                            @foreach($allResults as $result)
                                                                <tr>
                                                                    <td>{{$result->user->name}}
                                                                        ({{$result->user->email}})
                                                                    </td>
                                                                    <td>{{$result->topic->title}}</td>
                                                                    <td>{{$result->created_at}}</td>
                                                                    <td>{{$result->getPercent()}}%</td>
                                                                    <td>{{date('i:s', strtotime($result->finished_at))}}</td>
                                                                    <td>
                                                                        <a href="{{route('results.show', $result->id)}}"
                                                                           class="btn btn-xs btn-primary">View</a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- /#page-content-wrapper -->

            </div>
            <!-- /#wrapper -->


        </div>
    </div>
</x-app-layout>
