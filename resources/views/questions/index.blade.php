<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('All Questions') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="d-flex" id="wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 mt-4">
                                <table class="table table-bordered table-striped datatable">
                                    <thead>
                                    <tr>
                                        <th>Topic</th>
                                        <th>Question text</th>
                                        <th>More</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach($questions as $question)
                                        <tr>
                                            <td>
                                                @if($question->topic)
                                                    {{$question->topic->title}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(strlen($question->text) > 100)
                                                    {{substr($question->text, 0, 100)}}...
                                            </td>
                                            @else
                                                {{$question->text}}
                                            @endif
                                            <td>
                                                <a href="{{route('questions.show', $question->id)}}"
                                                   class="btn btn-xs btn-primary">View</a>
                                                <a href="{{route('questions.edit', $question->id)}}"
                                                   class="btn btn-xs btn-warning">Edit</a>
                                                <form class="inline-flex"
                                                      action="{{route('questions.destroy', $question->id)}}"
                                                      method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button class="btn btn-xs btn-danger" type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
