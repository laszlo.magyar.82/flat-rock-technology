<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Question') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="d-flex" id="wrapper">
                    <div class="container">
                        @if($question)
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            <div class="form-group">
                                <hr>

                                <form method="POST" action="{{route('questions.update', $question->id)}}">
                                    @csrf
                                    @method('put')
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <select class="form-control" name="topic_id">
                                                        @foreach($topics as $topic)
                                                            <option
                                                                value="{{$topic->id}}" {{($topic->id == $question->topic_id) ? 'selected' : ''}}>{{$topic->title}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 form-group">
                                                    <label for="text" class="control-label">Question
                                                        text</label>
                                                    <textarea class="form-control" required placeholder=""
                                                              name="text"
                                                              cols="50"
                                                              rows="10">{{$question->text}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Update">
                                </form>
                            </div>
                        @else
                            <h1>No Question</h1>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
