<?php

namespace Database\Seeders;

use App\Models\Option;
use App\Models\Question;
use Illuminate\Database\Seeder;

class QuestionsSeeder extends Seeder
{
    const TOPIC_ID = 1;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $questions = [
            'What does PHP stand for?' => [
                ['Personal Hypertext Processor', 0, 0],
                ['Private Home Page', 0, 0],
                ['PHP: Hypertext Preprocessor', 1, 0],
            ],
            'PHP server scripts are surrounded by delimiters, which?' => [
                ['<script>...</script>', 0, 0],
                ['<?php...?>', 1, 0],
                ['<?php>...</?>', 0, 0],
            ],
            'How do you write "Hello World" in PHP' => [
                ['echo "Hello World";', 1, 0],
                ['"Hello World";', 0, 0],
                ['Document.write("Hello World")', 0, 0]
            ],
            'Which of the following is used to declare a constant' => [
                ['const', 1, 0],
                ['define', 0, 0],
                ['constant', 0, 0],
                ['#pragma', 0, 0],
            ],
            'Which of the following is the way to create comments in PHP?' => [
                ['// commented code to end of line', 0, 0],
                ['/* commented code here */', 0, 0],
                ['# commented code to end of line', 0, 0],
                ['all of the above', 1, 0],
            ],
            'Which of the following is NOT a valid PHP comparison operator?' => [
                ['!=', 1, 0],
                ['>=', 0, 0],
                ['<=>', 0, 0],
                ['<>', 0, 0],
            ],
            'Which of the following is NOT a magic predefined constant?' => [
                ['__LINE__', 0, 0],
                ['__FILE__', 1, 0],
                ['__DATE__', 0, 0],
                ['__CLASS__', 0, 0],
            ],
            'What is the strpos() function used for?' => [
                ['Find the last character of a string', 0, 0],
                ['Search for character within a string', 0, 0],
                ['Locate position of a string\'s first character', 1, 0],
            ],
            'What is the difference between GET and POST method?' => [
                ['GET displays the form values entered in the URL of the address bar where as POST does not.', 1, 0],
                ['POST displays the form values entered in the URL of the address bar where as GET does not.', 0, 0],
                ['There is no difference', 0, 0]
            ],
            'What is array_keys() used for?' => [
                ['Compares array keys, and returns the matches', 0, 0],
                ['Checks if the specified key exists in the array', 0, 0],
                ['Returns all the keys of an array', 1, 0],
            ]
        ];

        foreach ($questions as $question => $options) {
            $q = new Question(['topic_id' => self::TOPIC_ID, 'text' => $question]);
            $q->save();
            foreach ($options as $option) {
                $o = new Option([
                                    'question_id' => $q->id,
                                    'option' => $option[0],
                                    'correct' => $option[1],
                                    'binary' => $option[2]
                                ]);
                $o->save();
            }
        }
    }
}
