## Set up

Clone the repository

```git clone https://gitlab.com/laszlo.magyar.82/flat-rock-technology```

Set environment variables

```cp .env.example .env```

Start the container

```docker compose up -d --build```

Download the packages

```docker compose exec php composer install```

## Initialization

Prepare the database

```docker compose exec php php artisan key:generate```

```docker compose exec php php artisan migrate:fresh```

```docker compose exec php php artisan db:seed```

You can now log in into the application

| User Type     | Email             | Password |
|---------------|-------------------|----------|
| User          | user@example.com  | password |
| Administrator | admin@example.com | password |


## Testing

1. Setup test environment

```docker compose exec php php artisan key:generate --env="testing"```

```docker compose exec php php artisan migrate --database=sqlite --env=testing```

2. Run tests

Unit and feature tests
```docker compose exec php ./vendor/bin/phpunit```

Static analysis
```docker compose exec php composer sa```
