<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class isAdmin
{
    public function handle($request, Closure $next): Closure|RedirectResponse|Response
    {
        if (Auth::user()) {
            if ('admin' == Auth::user()->role) {
                return $next($request);
            } else {
                return redirect()->back();
            }
        } else {
            return redirect()->route('login');
        }
    }
}
