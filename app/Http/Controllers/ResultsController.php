<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreResultRequest;
use App\Models\Option;
use App\Models\Question;
use App\Models\Result;
use App\Models\Topic;
use App\Models\UserAnswer;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class ResultsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin')->except(['store', 'show', 'score']);
    }

    public function index(): Factory|View|Application
    {
        $allResults = Result::orderBy('created_at', 'desc')->get();

        return view('results.index', ['allResults' => $allResults]);
    }

    public function score(): Factory|View|Application
    {
        $allResults = Result::orderBy('score_percent', 'desc')->orderBy('finished_at', 'asc')->get();

        return view('results.score', ['allResults' => $allResults]);
    }

    public function store(StoreResultRequest $request): Redirector|Application|RedirectResponse
    {
        $score = 0;
        $questions = $request->input('option');
        $allQuestionsFromTopic = Question::where('topic_id', $request->input('topic_id'))->get();

        $topic = Topic::find($request->input('topic_id'));
        $key = $topic->getSessionKey(Auth::user()->id);
        $started = $request->session()->get($key, null);

        if ($questions) {
            foreach ($questions as $key => $value) {
                $question = Question::find($key);
                $options = [];
                foreach (Option::where('question_id', $question->id)->get() as $option) {
                    $options[$option->id] = $option;
                }
                $userCorrectAnswers = 0;
                foreach ($value as $answerKey => $answerValue) {
                    if (isset($options[$answerValue]) && 1 === $options[$answerValue]->correct) {
                        ++$userCorrectAnswers;
                    } else {
                        --$userCorrectAnswers;
                    }
                }
                if ($question->correctOptionsCount() == $userCorrectAnswers) {
                    ++$score;
                }
            }
            $result = new Result();
            $result->user_id = Auth::user()->id;
            $result->topic_id = $request->input('topic_id');
            $result->correct_answers = $score;
            $result->questions_count = count($request->input('question_id'));
            $result->unanswered_questions = count($allQuestionsFromTopic) - count($questions);
            $result->score_percent = $result->getPercent();
            $result->finished_at = date('Y-m-d H:i:s', time() - $started);
            $result->save();

            foreach ($questions as $key => $value) {
                foreach ($value as $answerKey => $answerValue) {
                    $userOption = new UserAnswer();
                    $userOption->user_id = Auth::user()->id;
                    $userOption->result_id = $result->id;
                    $userOption->question_id = $key;
                    $userOption->topic_id = $request->input('topic_id');
                    $userOption->option_id = $answerValue;
                    $userOption->save();
                }
            }
        } else {
            $result = new Result();
            $result->user_id = Auth::user()->id;
            $result->topic_id = $request->input('topic_id');
            $result->correct_answers = 0;
            $result->questions_count = count($request->input('question_id'));
            $result->unanswered_questions = count($allQuestionsFromTopic);
            $result->score_percent = 0;
            $result->save();
        }

        $request->session()->put([$key => null]);

        return redirect(route('results.show', $result->id));
    }

    public function show(int $id): Factory|View|Application
    {
        $result = Result::find($id);

        return view('results.show', ['result' => $result]);
    }
}
