<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateQuestionOptionsRequest;
use App\Models\Option;
use App\Models\Question;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class QuestionsOptionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(): Factory|View|Application
    {
        $options = Option::all();

        return view('options.index', ['options' => $options]);
    }

    public function show(int $id): Factory|View|Application
    {
        $option = Option::find($id);

        return view('options.show', ['option' => $option]);
    }

    public function edit(int $id): Factory|View|Application
    {
        $questions = Question::all();
        $option = Option::find($id);

        return view('options.edit', ['option' => $option, 'questions' => $questions]);
    }

    public function update(UpdateQuestionOptionsRequest $request, int $id): Redirector|Application|RedirectResponse
    {
        $option = Option::find($id);

        $option->question_id = $request->input('question_id');
        $option->option = $request->input('option');
        $option->correct = $request->input('correct');
        $option->save();

        return redirect(route('options.index'));
    }

    public function destroy(int $id): Redirector|Application|RedirectResponse
    {
        $option = Option::find($id);
        $option->delete();

        return redirect(route('options.index'));
    }
}
