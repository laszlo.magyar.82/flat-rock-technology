<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTopicRequest;
use App\Http\Requests\UpdateTopicRequest;
use App\Models\Topic;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class TopicController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin')->except(['index', 'show']);
    }

    public function index(): Factory|View|Application
    {
        $topics = Topic::all();

        return view('topics.index', ['topics' => $topics]);
    }

    public function create(): Factory|View|Application
    {
        $topics = Topic::all();

        return view('topics.create', ['topics' => $topics]);
    }

    public function store(StoreTopicRequest $request): Redirector|Application|RedirectResponse
    {
        $topic = new Topic();
        $topic->title = $request->input('title');
        $topic->save();

        return redirect(route('topics.edit', ['topic' => $topic->id]));
    }

    public function show(Request $request, int $id): Factory|View|Application
    {
        $topic = Topic::find($id);
        $key = $topic->getSessionKey(Auth::user()->id);
        $started = $request->session()->get($key, null);

        if (empty($started)) {
            $request->session()->put([$key => time()]);
            $started = $request->session()->get($key);
        }

        return view('topics.show', ['topic' => $topic, 'deadline' => Topic::getDeadLine($started)]);
    }

    public function edit(int $id): Factory|View|Application
    {
        $topic = Topic::find($id);

        return view('topics.edit', ['topic' => $topic]);
    }

    public function update(UpdateTopicRequest $request, int $id): Redirector|Application|RedirectResponse
    {
        $topic = Topic::find($id);
        $topic->title = $request->input('title');
        $topic->save();

        return redirect(route('topics.index'));
    }

    public function destroy(int $id): Redirector|Application|RedirectResponse
    {
        $topic = Topic::find($id);
        $topic->delete();

        return redirect(route('topics.index'));
    }
}
