<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(): Factory|View|Application
    {
        $users = User::all();

        return view('users.index', ['users' => $users]);
    }

    public function show(int $id): Factory|View|Application
    {
        $user = User::find($id);

        return view('users.show', ['user' => $user]);
    }

    public function edit(int $id): Factory|View|Application
    {
        $user = User::find($id);

        return view('users.edit', ['user' => $user]);
    }

    public function update(UpdateUserRequest $request, $id): Redirector|Application|RedirectResponse
    {
        $user = User::find($id);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');
        $user->save();

        return redirect(route('users.index'));
    }

    public function destroy($id): Redirector|Application|RedirectResponse
    {
        $user = User::find($id);
        $user->delete();

        return redirect()->back();
    }
}
