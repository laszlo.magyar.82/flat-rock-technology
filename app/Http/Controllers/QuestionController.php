<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreQuestionRequest;
use App\Http\Requests\UpdateQuestionRequest;
use App\Models\Option;
use App\Models\Question;
use App\Models\Topic;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index(): Factory|View|Application
    {
        $questions = Question::all();

        return view('questions.index', ['questions' => $questions]);
    }

    public function store(StoreQuestionRequest $request): Redirector|Application|RedirectResponse
    {
        $topicID = $request->input('topic');
        $questionText = $request->input('question');
        $optionArray = $request->input('options');
        $correctOptions = $request->input('correct');
        $binaryOptions = $request->input('binary');

        $question = new Question();
        $question->topic_id = $topicID;
        $question->text = $questionText;
        $question->save();

        $questionToAdd = Question::latest()->first();
        $questionID = $questionToAdd->id;

        foreach ($optionArray as $index => $opt) {
            $option = new Option();
            $option->question_id = $questionID;
            $option->option = $opt;
            foreach ($correctOptions as $correctOption) {
                if ($correctOption == $index + 1) {
                    $option->correct = 1;
                }
            }
            if (!empty($binaryOption)) {
                foreach ($binaryOptions as $binaryOption) {
                    if ($binaryOption == $index + 1) {
                        $option->binary = 1;
                    }
                }
            }

            $option->save();
        }

        return redirect()->back();
    }

    public function show(int $id): Factory|View|Application
    {
        $question = Question::find($id);

        return view('questions.show', ['question' => $question]);
    }

    public function edit(int $id): Factory|View|Application
    {
        $question = Question::find($id);
        $topics = Topic::all();

        return view('questions.edit', ['question' => $question, 'topics' => $topics]);
    }

    public function update(UpdateQuestionRequest $request, int $id): Redirector|Application|RedirectResponse
    {
        $topicID = $request->input('topic_id');
        $questionText = $request->input('text');

        $question = Question::find($id);
        $question->topic_id = $topicID;
        $question->text = $questionText;
        $question->save();

        return redirect(route('questions.index'));
    }

    public function destroy(int $id): Redirector|Application|RedirectResponse
    {
        $question = Question::find($id);
        $question->delete();

        return redirect(route('questions.index'));
    }
}
