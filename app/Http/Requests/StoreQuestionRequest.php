<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'topic' => 'required|exists:topics,id',
            'question' => 'required',
            'options' => 'required',
            'correct' => 'required',
        ];
    }
}
