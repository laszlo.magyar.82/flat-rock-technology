<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Result extends Model
{
    protected $table = 'results';

    public function topic(): BelongsTo
    {
        return $this->belongsTo('App\Models\Topic');
    }

    public function user(): HasOne
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function options(): HasMany
    {
        return $this->hasMany('App\Models\UserAnswer', 'result_id', 'id');
    }

    public function getPercent(): float
    {
        return round((float) ($this->correct_answers / $this->questions_count) * 100, 2);
    }
}
