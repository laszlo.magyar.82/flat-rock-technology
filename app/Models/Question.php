<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = ['text', 'topic_id'];

    public function options(): HasMany
    {
        return $this->hasMany('App\Models\Option', 'question_id', 'id');
    }

    public function correctOptionsCount(): int
    {
        return $this->options()->where('correct', 1)->count();
    }

    public function correctOptions(): Collection
    {
        return $this->options()->where('correct', 1)->get();
    }

    public function topic(): HasOne
    {
        return $this->hasOne('App\Models\Topic', 'id', 'topic_id');
    }
}
