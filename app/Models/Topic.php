<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Topic extends Model
{
    public const TIMOUT = 600;
    protected $table = 'topics';

    protected $fillable = ['title'];

    public static function getDeadLine(int $started): string
    {
        return date('Y-m-d H:i:s', $started + self::TIMOUT);
    }

    public function questions(): HasMany
    {
        return $this->hasMany('App\Models\Question', 'topic_id', 'id');
    }

    public function getSessionKey(int $userId): string
    {
        return 'startTopic_'.$userId.'_'.$this->id;
    }
}
