<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Option extends Model
{
    protected $table = 'questions_options';
    protected $fillable = ['option', 'correct', 'binary', 'question_id'];

    public function question(): HasOne
    {
        return $this->hasOne('App\Models\Question', 'id', 'question_id');
    }
}
