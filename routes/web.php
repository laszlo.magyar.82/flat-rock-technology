<?php

use App\Http\Controllers\QuestionController;
use App\Http\Controllers\QuestionsOptionsController;
use App\Http\Controllers\ResultsController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', [TopicController::class, 'index'])->name('dashboard');

Route::resource('users', UserController::class);
Route::resource('topics', TopicController::class);
Route::resource('questions', QuestionController::class);
Route::resource('options', QuestionsOptionsController::class);
Route::get('/results/score', [ResultsController::class, 'score'])->name('results.score');
Route::resource('results', ResultsController::class);


require __DIR__ . '/auth.php';
